//
//  ContentView.swift
//  RM
//
//  Created by Carl Waldorf on 02.03.24.
//

import SwiftUI

struct ContentView: View {
    @State private var selectedTab = 2
    var body: some View {
        
        VStack {
            TabView(selection: $selectedTab) {
                Text("OK")
                    .tabItem {
                        Label("Gerichte", systemImage: "tray.full")
                    }
                    .tag(0)
                
                Text("OK")
                    .tabItem {
                        Label("Getränke", systemImage: "mug")
                    }
                    .tag(1)
                // BedienungsTabd
                BedienenView()
                    .tabItem {
                        Label("Bedienen", systemImage: "menucard")
                    }
                    .tag(2)
            }
            .accentColor(.blue)
        }
    }
}

#Preview {
    ContentView()
}
