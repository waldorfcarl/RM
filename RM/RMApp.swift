//
//  RMApp.swift
//  RM
//
//  Created by Carl Waldorf on 02.03.24.
//

import SwiftUI
import Firebase

@main
struct RMApp: App {
    
    init() {
        FirebaseApp.configure()
    }
    
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
