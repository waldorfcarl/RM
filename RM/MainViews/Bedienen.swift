//
//  Bedienen.swift
//  RM
//
//  Created by Carl Waldorf on 02.03.24.
//

import Foundation
import SwiftUI

struct BedienenView: View {
    var body: some View {
        GeometryReader { geometry in

            NavigationView {
                VStack{
                    
                    List {
                        // Section
                        NavigationLink(destination: BestellungView()) {
                        Section {

                            Button {
                            } label: {
                                
                                    Text("Bestellung aufnehmen")
                                
                                }
                                
                            }
                            
                        }
                        
                        
                        NavigationLink(destination: AbrechnungView()) {
                            Section {
                                Button {
                                } label: {
                                        Text("Abrechnen")
                                        
                                    
                                }
                            }
                            
                        }
                      
                        
                        
                    }
                    .listStyle(.insetGrouped)
                }
                .navigationTitle("Bedienen")
                .navigationBarBackButtonHidden(true)
            }
        }
    }
}

#Preview {
    BedienenView()
}
