import SwiftUI
import Foundation
import Firebase
import FirebaseFirestore


class OverviewViewModel: ObservableObject {
    @Published var korbItems: [KorbItem] = []
    @Published var isLoading = false
    @Published var error: Error?
    
    func fetchData(forTable tischnummer: String) {
        isLoading = true
        let db = Firestore.firestore()
        let korbCollection = db.collection("Korb")
        
        // Query documents only for the specified table
        korbCollection.whereField("tischnummer", isEqualTo: tischnummer).getDocuments { snapshot, error in
            defer {
                self.isLoading = false
            }
            
            if let error = error {
                self.error = error
                print("Error fetching data: \(error.localizedDescription)")
                return
            }
            
            if let documents = snapshot?.documents {
                self.korbItems = documents.compactMap { document in
                    do {
                        let data = document.data()
                        let tischnummer = data["tischnummer"] as? String ?? ""
                        let selectedDish = data["selectedDish"] as? String ?? ""
                        let dishQuantity = data["dishQuantity"] as? Int ?? 0
                        let selectedDrink = data["selectedDrink"] as? String ?? ""
                        let drinkQuantity = data["drinkQuantity"] as? Int ?? 0
                        
                        let korbItem = KorbItem(
                            tischnummer: tischnummer,
                            selectedDish: selectedDish,
                            dishQuantity: dishQuantity,
                            selectedDrink: selectedDrink,
                            drinkQuantity: drinkQuantity
                        )
                        
                        print("Fetched KorbItem: \(korbItem)")
                        
                        return korbItem
                    } catch {
                        self.error = error
                        print("Error decoding data for document \(document.documentID): \(error.localizedDescription)")
                        return nil
                    }
                }
            }
        }
    }
}


struct OverviewView: View {
    @ObservedObject private var viewModel: OverviewViewModel
    
    var tischnummer: String // Provide the table name
    
    init(viewModel: OverviewViewModel, tischnummer: String) {
        self.viewModel = viewModel
        self.tischnummer = tischnummer
        viewModel.fetchData(forTable: tischnummer) // Fetch data for the specific table on initialization
    }
    
    var body: some View {
        List {
            ForEach(viewModel.korbItems) { korbItem in
                Section(){
                    Text(korbItem.tischnummer)
                    Text("\(korbItem.dishQuantity) x \(korbItem.selectedDish)")
                    Text("\(korbItem.drinkQuantity) x \(korbItem.selectedDrink)")
                }
            }
        }
        .navigationTitle("Korb für \(tischnummer)")
    }
}


