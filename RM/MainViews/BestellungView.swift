import SwiftUI
import Firebase
import FirebaseFirestore
import FirebaseFirestoreSwift

struct BestellungView: View {
    @ObservedObject private var viewModel: TableViewModel

    init(viewModel: TableViewModel = TableViewModel()) {
        self.viewModel = viewModel
    }

    var body: some View {
            List(viewModel.data) { item in
                NavigationLink(destination: TableActionView(tischnummer: item.tischnummer)) {
                    VStack(alignment: .leading) {
                        Text("\(item.tischnummer)")
                            .font(.headline)
                        Text("Tisch ist \(item.besetzt ? "besetzt" : "frei")")
                            .foregroundColor(item.besetzt ? Color.red : Color.green)
                            .font(.subheadline)
                            .foregroundColor(.gray)
                    }
                    .padding(8)
                }
            }
            .navigationBarTitle("Tische")
    }


    // Functions
    func triggerFeedback() {
        let generator = UIImpactFeedbackGenerator(style: .light)
        generator.prepare()
        generator.impactOccurred()
    }
    
}


// ViewModel for fetching data
class TableViewModel: ObservableObject {
    @Published var data: [YourDataModel] = []
    @Published var isLoading = false
    @Published var error: Error?
    
    init() {
        fetchData()
    }
    
    func fetchData() {
        isLoading = true
        let db = Firestore.firestore()
        db.collection("Tische").getDocuments { snapshot, error in
            defer {
                self.isLoading = false
            }
            
            if let error = error {
                self.error = error
                print("Error fetching data: \(error.localizedDescription)")
                return
            }
            
            if let documents = snapshot?.documents {
                self.data = documents.compactMap { document in
                    do {
                        let id = document.documentID
                        let besetzt = try document.get("Besetzt") as? Bool ?? false
                        let tischnummer = try document.get("Tischnummer") as? String ?? ""
                        
                        return YourDataModel(id: id, besetzt: besetzt, tischnummer: tischnummer)
                    } catch {
                        self.error = error
                        print("Error decoding data for document \(document.documentID): \(error.localizedDescription)")
                        return nil
                    }
                }
            }
        }
    }
}




struct YourDataModel: Identifiable {
    var id: String
    var besetzt: Bool
    var tischnummer: String
    
    init(id: String, besetzt: Bool, tischnummer: String) {
        self.id = id
        self.besetzt = besetzt
        self.tischnummer = tischnummer
    }
}



#Preview {
    BestellungView()
}

