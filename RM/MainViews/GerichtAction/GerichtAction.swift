//
//  GerichtAction.swift
//  RM
//
//  Created by Carl Waldorf on 03.03.24.
//

import SwiftUI
import Firebase
import FirebaseFirestore
import FirebaseFirestoreSwift
import UIKit

struct GerichtActionView: View {
    
    
    var gerichte: String
    var tischnummer: String
    
    @State private var anzahlGericht = 1
    @State private var anzahlGetränk = 0
    @ObservedObject private var viewModel: GetränkeViewModel
    @State private var includeDrinks = false // Add a state variable for including drink
    @State private var selectedDrinks: [GetränkeDataModel] = []
    @State private var showStepper = false
    @State private var arrowHeading = false
    @State private var selectedDrink: GetränkeDataModel? = nil
    @State private var isOverviewPresented = false
    
    
    
    init(gerichte: String, viewModel: GetränkeViewModel, tischnummer: String) {
        self.gerichte = gerichte
        self.viewModel = viewModel
        self.tischnummer = tischnummer
    }

    
    func saveToFirebase() {
        let korbItem = KorbItem(
            tischnummer: tischnummer,
            selectedDish: gerichte,
            dishQuantity: anzahlGericht,
            selectedDrink: selectedDrink?.name ?? "Kein Getränk",
            drinkQuantity: anzahlGetränk
        )

        let db = Firestore.firestore()
        let korbCollection = db.collection("Korb \(tischnummer)")

        let gerichtReference = korbCollection.document(korbItem.selectedDish)
        let getränkReference = korbCollection.document(korbItem.selectedDrink)

        korbCollection.document(korbItem.selectedDish).getDocument { (document, error) in
            if let error = error {
                print("Error fetching document: \(error.localizedDescription)")
                // Handle the error accordingly
            } else {
                if let existingData = document?.data() {
                    // Document with the same tischnummer exists, update specific fields

                    // Create a new field for the selected dish with quantity as a string
                    let newFieldKey = "\(korbItem.selectedDish)"
                    let newFieldValue = "\(korbItem.dishQuantity) mal \(korbItem.selectedDish)"

                    // Increment dish and drink quantities
                    let updatedGerichtAnzahl = (existingData["Gericht Anzahl"] as? Int ?? 0) + korbItem.dishQuantity
                    let updatedGetränkAnzahl = (existingData["Getränk Anzahl"] as? Int ?? 0) + korbItem.drinkQuantity

                    // Update the document in Firestore
                    gerichtReference.setData([
                        newFieldKey: newFieldValue,
                        "Gericht Anzahl": updatedGerichtAnzahl,
                    ]) { error in
                        if let error = error {
                            print("Error updating Gericht document: \(error.localizedDescription)")
                            // Handle the error accordingly
                        } else {
                            print("Gericht Document updated successfully.")
                            // Optionally, you can perform any action after successfully updating the document
                        }
                    }

                    getränkReference.updateData([
                        "\(korbItem.selectedDrink)": "\(korbItem.drinkQuantity) mal \(korbItem.selectedDrink)",
                        "Getränk Anzahl": updatedGetränkAnzahl,
                    ]) { error in
                        if let error = error {
                            print("Error updating Getränk document: \(error.localizedDescription)")
                            // Handle the error accordingly
                        } else {
                            print("Getränk Document updated successfully.")
                        }
                    }
                } else {
                    // Document with the same tischnummer doesn't exist, add a new document
                    let gerichtData: [String: Any] = [
                        "\(korbItem.selectedDish)": "\(korbItem.dishQuantity) mal \(korbItem.selectedDish)",
                        "Gericht Anzahl": korbItem.dishQuantity
                    ]

                    let getränkData: [String: Any] = [
                        "\(korbItem.selectedDrink)": "\(korbItem.drinkQuantity) mal \(korbItem.selectedDrink)",
                        "Getränk Anzahl": korbItem.drinkQuantity,
                    ]

                    // Add data to Firestore
                    gerichtReference.setData(gerichtData) { error in
                        if let error = error {
                            print("Error adding Gericht document: \(error.localizedDescription)")
                            // Handle the error accordingly
                        } else {
                            print("Gericht Document added successfully.")
                            // Optionally, you can perform any action after successfully adding the document
                        }
                    }

                    getränkReference.setData(getränkData) { error in
                        if let error = error {
                            print("Error adding Getränk document: \(error.localizedDescription)")
                            // Handle the error accordingly
                        } else {
                            print("Getränk Document added successfully.")
                        }
                    }
                }
            }
        }
    }



    
    
    var body: some View {
        List {
            Section {
                Stepper(" \(anzahlGericht) x \(gerichte)", value: $anzahlGericht, in: 1...10)
                    .padding()
                if anzahlGericht == 10 {
                    Text("Maximale Anzahl für ein Gericht")
                        .font(.callout)
                        .foregroundColor(.red)
                }
            }
            
            Section(header: Text("Getränke")) {
                Button {
                    if includeDrinks {
                        includeDrinks = false
                    } else {
                        includeDrinks = true
                    }
                } label: {
                    HStack{
                        Text("Getränke zur Bestellung hinzufügen")
                            .foregroundStyle(Color.black)
                        
                        Spacer()
                        
                        if includeDrinks {
                            Image(systemName: "arrow.up")
                                .foregroundStyle(Color.black)
                        }
                        else {
                            Image(systemName: "arrow.down")
                                .foregroundStyle(Color.black)
                        }
                        
                    }
                }
                
                
                if includeDrinks {
                    ForEach(viewModel.data.indices, id: \.self) { index in
                        let drink = viewModel.data[index]
                        
                        HStack {
                            // Show a checkmark icon for selected drinks
                            Image(systemName: selectedDrink?.id == drink.id ? "checkmark.circle.fill" : "circle")
                                .foregroundColor(.green)
                                .font(.system(size: 24))
                            
                            VStack(alignment: .leading) {
                                Text("\(drink.name)")
                                    .font(.headline)
                                Text("\(drink.preis)€")
                                    .font(.subheadline)
                                    .foregroundColor(.gray)
                            }
                        }
                        .contentShape(Rectangle())
                        .onTapGesture {
                            // Toggle the selected state of the drink
                            if selectedDrink?.id == drink.id {
                                selectedDrink = nil
                                showStepper = false
                            } else {
                                selectedDrink = drink
                                showStepper = true
                                print(selectedDrink?.name ?? "Nichts")
                            }
                        }
                    }
                }
                
                if let selectedDrink = selectedDrink {
                    if showStepper {
                        Section {
                            Stepper("\(selectedDrink.name) - Menge: \(anzahlGetränk)", value: $anzahlGetränk, in: 1...10)
                        }
                        
                    }
                }
            }
            
            
            
            Section {
                Button(action: {
                    vibrateForSuccess()
                    saveToFirebase()
                    isOverviewPresented.toggle()
                }) {
                    HStack {
                        Image(systemName: "cart")
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .frame(width: 25, height: 25)
                        Text("\(gerichte) in Korb packen")
                    }
                }
            }
            
            .sheet(isPresented: $isOverviewPresented) {
                AddToCart(tischnummer: tischnummer)
            }
        }
        .onAppear {
            viewModel.fetchData()
        }
        .navigationTitle(gerichte)
    }
    func vibrateForSuccess() {
        if #available(iOS 10.0, *) {
            let generator = UINotificationFeedbackGenerator()
            generator.notificationOccurred(.success)
        } else {
            // Fallback for earlier versions
            let generator = UINotificationFeedbackGenerator()
            generator.notificationOccurred(.success)
        }
    }
}




class GetränkeViewModel: ObservableObject {
    @Published var data: [GetränkeDataModel] = []
    @Published var isLoading = false
    @Published var error: Error?
    
    func fetchData() {
        isLoading = true
        let db = Firestore.firestore()
        db.collection("Getränke").getDocuments { snapshot, error in
            defer {
                self.isLoading = false
            }
            
            if let error = error {
                self.error = error
                print("Error fetching data: \(error.localizedDescription)")
                return
            }
            
            if let documents = snapshot?.documents {
                self.data = documents.compactMap { document in
                    do {
                        let id = document.documentID
                        let name = try document.get("Name") as? String ?? ""
                        let preis = try document.get("Preis") as? String ?? ""
                        
                        return GetränkeDataModel(id: id, name: name, preis: preis)
                    } catch {
                        self.error = error
                        print("Error decoding data for document \(document.documentID): \(error.localizedDescription)")
                        return nil
                    }
                }
            }
        }
    }
}

struct GetränkeDataModel: Identifiable {
    var id: String
    var name: String
    var preis: String
}

class KorbItem: Identifiable {
    var id: UUID
    var tischnummer: String
    var selectedDish: String
    var dishQuantity: Int
    var selectedDrink: String
    var drinkQuantity: Int

    init(tischnummer: String, selectedDish: String, dishQuantity: Int, selectedDrink: String, drinkQuantity: Int) {
        self.id = UUID() // Assign a unique identifier to the id property
        self.tischnummer = tischnummer
        self.selectedDish = selectedDish
        self.dishQuantity = dishQuantity
        self.selectedDrink = selectedDrink
        self.drinkQuantity = drinkQuantity
    }
}


#Preview {
    GerichtActionView(gerichte: "Feta Käse", viewModel: GetränkeViewModel(), tischnummer: "Tisch 01")
}
