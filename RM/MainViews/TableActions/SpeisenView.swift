//
//  SpeisenView.swift
//  RM
//
//  Created by Carl Waldorf on 02.03.24.
//

import SwiftUI
import Firebase
import FirebaseFirestore


struct SpeisenView: View {
    
    @ObservedObject private var viewModel: SpeisenViewModel
    @State private var searchText = ""
    @State private var quantity = 1
    
    var tischnummer: String
    
    
    init(viewModel: SpeisenViewModel = SpeisenViewModel(), tischnummer: String) {
        self.viewModel = viewModel
        self.tischnummer = tischnummer
    }
    
    var filteredData: [String: [SpeisenDataModel]] {
        guard !searchText.isEmpty else {
            return viewModel.groupedData
        }
        
        return viewModel.groupedData.mapValues { group in
            group.filter { $0.name.localizedCaseInsensitiveContains(searchText) }
        }.filter { !$0.value.isEmpty }
    }
    
    
    
    var body: some View {
        List {
            Section(header: Text("Ausgewählte Speisen")){
                
            }
            
            ForEach(filteredData.keys.sorted(), id: \.self) { group in
                Section(header: Text(group)) {
                    ForEach(viewModel.groupedData[group]!) { item in
                        Button(action: {
                            // Handle the click on the whole object
                            self.triggerVibration()
                            handleSelectedItem(selectedItem: [
                                "Gericht": item.name,
                                "Menge": item.quantity,
                            ])
                        }) {
                            HStack {
                                // Gericht Name & Preis
                                VStack(alignment: .leading, spacing: 8) {
                                    Text("\(item.quantity) x \(item.name)")
                                        .font(.headline)
                                    Text("Preis: \(item.preis)€")
                                        .font(.subheadline)
                                        .foregroundColor(.gray)
                                }
                                .padding(8)
                                
                                // Stepper für Mengenangabe
                                HStack {
                                    Button(action: {}) {
                                        EmptyView()
                                    }
                                    .buttonStyle(PlainButtonStyle()) // This removes the button style
                                    
                                    
                                        Stepper("\(quantity)", value: $quantity)
                                            .onChange(of: quantity) { newValue in
                                                // Trigger the vibration when the Stepper value changes
                                                self.triggerVibration()
                                            }
                                    }
                                
                            }
                        }
                    }
                }
            }
        }
        .navigationBarTitle("Speisen")
        .searchable(text: $searchText, prompt: "Gerichte Suchen")
        .onAppear {
            viewModel.fetchData()
        }
    }
    
    func triggerVibration() {
            let impactFeedbackgenerator = UIImpactFeedbackGenerator(style: .light)
            impactFeedbackgenerator.prepare()
            impactFeedbackgenerator.impactOccurred()
    }
    
    func handleSelectedItem(selectedItem: [String: Any]) {
           if let Name = selectedItem["Gericht"] as? String,
              let quantity = selectedItem["Quantity"] as? Int {

               let db = Firestore.firestore()
               let korbCollection = db.collection("Korb")

               let data: [String: Any] = [
                   "tischnummer": tischnummer,
                   "Gericht": Name,
                   "Quantity": quantity
               ]

               korbCollection.addDocument(data: data) { error in
                   if let error = error {
                       print("Error adding document to Korb: \(error)")
                   } else {
                       print("Document added to Korb successfully!")
                   }
               }
           }
       }
   }



struct SearchBar: View {
    @Binding var text: String

    var body: some View {
        HStack {
            TextField("Gericht suchen", text: $text)
                .padding(15)
                .cornerRadius(8)
                .padding([.leading, .trailing], 8)
                .padding([.top, .bottom], 4)
                .keyboardType(.default)
                .autocapitalization(.none)
                .disableAutocorrection(true)

            if !text.isEmpty {
                Button {
                    withAnimation {
                        text = ""
                        UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
                    }
                } label: {
                    Image(systemName: "xmark.circle.fill")
                        .foregroundColor(.gray)
                        .padding(.trailing, 5)
                }
                .transition(.scale)
                .animation(.default)
            }
        }
        .background(Color(.systemBackground))
    }
}

// ViewModel for fetching data
class SpeisenViewModel: ObservableObject {
    @Published var data: [SpeisenDataModel] = []
    @Published var isLoading = false
    @Published var error: Error?

    @Published var groupedData: [String: [SpeisenDataModel]] = [:]

    init() {
        fetchData()
    }

    func fetchData() {
        isLoading = true
        let db = Firestore.firestore()
        db.collection("Gerichte").getDocuments { snapshot, error in
        
            defer {
                self.isLoading = false
            }

            if let error = error {
                self.error = error
                print("Error fetching data: \(error.localizedDescription)")
                return
            }

            if let documents = snapshot?.documents {
                self.data = documents.compactMap { document in
                    do {
                        let id = document.documentID
                        let quantity = 1
                        let gruppe = try document.get("Gruppe") as? String ?? ""
                        let name = try document.get("Name") as? String ?? ""
                        let preis = try document.get("Preis") as? String ?? ""

                        return SpeisenDataModel(id: id, gruppe: gruppe, name: name, preis: preis, quantity: quantity)
                    } catch {
                        self.error = error
                        print("Error decoding data for document \(document.documentID): \(error.localizedDescription)")
                        return nil
                    }
                }
                self.groupedData = Dictionary(grouping: self.data, by: { $0.gruppe })
            }
        }
    }
}


struct SpeisenDataModel: Identifiable {
    var id: String
    var gruppe: String
    var name: String
    var preis: String
    var quantity: Int
    
    init(id: String, gruppe: String, name: String, preis: String, quantity: Int) {
        self.id = id
        self.gruppe = gruppe
        self.name = name
        self.preis = preis
        self.quantity = quantity
    }
}


#Preview {
    SpeisenView(viewModel: SpeisenViewModel(), tischnummer: "14")
}
