//
//  TableAction.swift
//  RM
//
//  Created by Carl Waldorf on 02.03.24.
//
import SwiftUI
import Firebase
import FirebaseFirestore
import FirebaseFirestoreSwift

// Modify TableActionView
struct TableActionView: View {
    var tischnummer: String

    @State private var totalAnzahl: Int = 0

    var body: some View {
        List {
            Section(header:
                HStack {
                    Spacer()
                Image(systemName: "cart")
                    .imageScale(.large)
                    .scaleEffect(1.5) // Adjust the scale factor as needed
                    .overlay(
                        Group {
                            if totalAnzahl > 0 {
                                Text("\(totalAnzahl)")
                                    .foregroundColor(.white)
                                    .font(.footnote)
                                    .padding(4)
                                    .background(Color.red)
                                    .clipShape(Circle())
                                    .offset(x: 10, y: -10) // Adjust the offset as needed
                            }
                        }
                    )
                }
            ){
            }
            .foregroundColor(.black)
            .imageScale(.large)

            Section(header: Label("Speisen", systemImage: "fork.knife.circle")){
                NavigationLink(destination: SpeisenView(tischnummer: tischnummer)) {
                    Text("Alle Gerichte")
                }

                Text("Andere Speisen")
                Text("Noch mehr Speisen")
            }

            Section(header: Label("Getränke", systemImage: "mug")) {
                Text("Getränke")
                Text("Andere Getränke")
                Text("Noch mehr Getränke")
            }
        }
        .navigationBarTitle(tischnummer)
        .onAppear {
            fetchData()
        }
    }

    private func fetchData() {
        let db = Firestore.firestore()
        let korbCollection = db.collection("Korb \(tischnummer)")

        // Fetch all documents from the collection
        korbCollection.getDocuments { (querySnapshot, error) in
            if let error = error {
                print("Error fetching documents: \(error.localizedDescription)")
                // Handle the error accordingly
            } else {
                // Calculate the total quantities of dishes and drinks
                self.totalAnzahl = querySnapshot?.documents.reduce(0, { (result, document) in
                    let gerichtAnzahl = document["Gericht Anzahl"] as? Int ?? 0
                    let getränkAnzahl = document["Getränk Anzahl"] as? Int ?? 0
                    return result + gerichtAnzahl + getränkAnzahl
                }) ?? 0
            }
        }
    }
}



#Preview {
    TableActionView(tischnummer: "Tisch 01")
}
