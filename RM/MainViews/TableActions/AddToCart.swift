import SwiftUI
import Firebase
import FirebaseFirestore

class SharedData: ObservableObject {
    @Published var gerichte: String = ""
    @Published var gerichteAnzahl: Int = 0
    @Published var selectedDrinkNames: [String] = []
}

struct AddToCart: View {
    var tischnummer: String = ""
    
    @State private var incrementingNumber = 0

    @StateObject private var sharedData = SharedData()

    @Environment(\.presentationMode) var presentationMode

    @State private var fetchedKorbData: [[String: Any]] = []

    var body: some View {
        List {
            Section(header: Text("Übersicht für \(tischnummer)").font(.title)) {
                // ... your existing code
            }

            Section(header: Text("Vorherige Korb Daten")) {
                ForEach(fetchedKorbData.indices, id: \.self) { index in
                    if let fetchedTischnummer = fetchedKorbData[index]["tischnummer"] as? String,
                       tischnummer == fetchedTischnummer {
                        displayDocumentFields(document: fetchedKorbData[index])
                            .font(.headline)
                    }
                }
            }
            .onAppear {
                fetchKorbDataFromFirestore()
            }

            Button(action: {
                addOrderToFirestore()
                presentationMode.wrappedValue.dismiss()
            }) {
                Text("Auftrag aufgeben")
                    .padding()
            }
        }
    }


    func displayDocumentFields(document: [String: Any]) -> some View {
        
            ForEach(document.sorted(by: { $0.key < $1.key }), id: \.key) { key, value in
                Text("\(String(describing: value))")
            }
        
    }



    func addOrderToFirestore() {
        let db = Firestore.firestore()
        let korbCollection = db.collection("Korb")

        let data: [String: Any] = [
            "tischnummer": tischnummer,
            "Gericht": sharedData.gerichte,
            "dishQuantity": sharedData.gerichteAnzahl,
            "selectedDrink": sharedData.selectedDrinkNames.joined(separator: ", "),
            "drinkQuantity": sharedData.selectedDrinkNames.count
        ]

        korbCollection.addDocument(data: data) { error in
            if let error = error {
                print("Error adding document: \(error)")
            } else {
                print("Document added successfully!")
            }
        }
    }

    func fetchKorbDataFromFirestore() {
        let db = Firestore.firestore()
        let korbCollection = db.collection("Korb")

        korbCollection.getDocuments { querySnapshot, error in
            if let error = error {
                print("Error getting documents: \(error)")
            } else {
                self.fetchedKorbData = querySnapshot?.documents.compactMap {
                    $0.data()
                } ?? []
            }
        }
    }
}

struct AddToCart_Previews: PreviewProvider {
    static var previews: some View {
        AddToCart(tischnummer: "Tisch 22")
    }
}
